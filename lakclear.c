
/*
 * delall.c - delete all samples/programs currently in the sampler's RAM 
 *
 * Copyright (c) Frank Neumann 2003
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "lakai.h"

int main(int argc, char *argv[])
{
	int i, numprogs, numsamps, res;
	LHANDLE hd1;
	LakaiProgramList lp;
	LakaiSampleList ls;
	
	if (argc !=2 )
	{
		fprintf(stderr, "Usage: %s <devname>\n", argv[0]);
		exit(1);
	}

	lakai_init();

	hd1 = lakai_open(argv[1]);
	if (hd1 >= 0)
	{
//		fprintf(stderr, "Open suceeded, handle: %d\n", hd1);
	}
	else
	{
		fprintf(stderr, "Open failed.\n");
		lakai_close(hd1);
		exit(5);
	}

//	fprintf(stderr, "############ GET SAMPLE NAMES: #############\n");
	numsamps = lakai_get_sample_list(hd1, &ls);
	if (numsamps > 0)
	{
		fprintf(stderr, "Deleting all samples...\n");
		for (i = numsamps-1; i >= 0; i--)
		{
			fprintf(stderr, "Delete sample #%d\n", i);
			res = lakai_delete_sample(hd1, i);
		}
		fprintf(stderr, "Done.\n");
	}
	else
		fprintf(stderr, "-- No resident sample names found. --\n");

	lakai_free_sample_list(&ls);
	
	/* delete all programs (and thus their keygroups) */
	numprogs = lakai_get_program_list(hd1, &lp);
	if (numprogs > 0)
	{
		fprintf(stderr, "Deleting all programs...\n");
		for (i = numprogs-1; i >= 0; i--)
		{
			fprintf(stderr, "Delete program #%d\n", i);
			res = lakai_delete_program(hd1, i);
		}
		fprintf(stderr, "Done.\n");
	}
	else
		fprintf(stderr, "-- No resident program names found. --\n");

	lakai_free_program_list(&lp);

	fprintf(stderr, "All done.\n");
	
	lakai_setmode(hd1, LAKAI_MODE_NORMAL);
	lakai_close(hd1);
	
	exit(0);
}

