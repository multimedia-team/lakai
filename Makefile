
# Makefile for lakai: link library and user programs
# Frank Neumann, February 2004
#

PREFIX=/usr/local

CC=gcc
CFLAGS=-g -Wall -Wstrict-prototypes # add -DDEBUG=1   for debugging
OBJS=lakai.o
LIB=liblakai.a

# These 3 programs should go into one binary some day.
all: $(LIB) lakclear lakbak lakres

progs: lakclear lakbak lakres

lakclear: lakclear.c $(LIB)
	$(CC) $< -o $@ -L. -llakai

lakbak: lakbak.c $(LIB)
	$(CC) $< -o $@ -L. -llakai

lakres: lakres.c $(LIB)
	$(CC) $< -o $@ -L. -llakai

$(LIB): $(OBJS)
	rm -f $@
	ar rcu $@ $<
	ranlib $@

lakai.o: lakai.c lakai.h
	$(CC) $(CFLAGS) -c $< -o $@

install: $(LIB) progs
	install -d $(PREFIX)/bin
	install -m 755 lakbak lakres lakclear $(PREFIX)/bin
	install -d $(PREFIX)/include
	install -m 644 lakai.h $(PREFIX)/include
	install -d $(PREFIX)/lib
	install -m 644 liblakai.a $(PREFIX)/lib
	
uninstall:
	rm -f $(PREFIX)/bin/lakbak
	rm -f $(PREFIX)/bin/lakres
	rm -f $(PREFIX)/bin/lakclear
	rm -f $(PREFIX)/include/lakai.h
	rm -f $(PREFIX)/lib/liblakai.a

clean:
	rm -f *.o $(OBJS) $(LIB) lakclear lakbak lakres

